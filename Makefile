all: clean

clean:
	-rm -f *~ README.html* cpp/*~ cpp/README.html* python/*~ python/README.html* 

clobber: clean
	-rm -f cpp/simple cpp/*.ass python/*.ass assets/ass/*.jpg
