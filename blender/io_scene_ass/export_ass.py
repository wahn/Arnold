# SPDX-License-Identifier: GPL-2.0-or-later

# Python modules
import math
import os
import platform
import sys
# Blender modules
import bpy
from mathutils import Matrix #, Vector, Color
from bpy_extras import node_shader_utils
#from bpy_extras import io_utils, node_shader_utils

# Arnold
arnoldFound = False
# hardcode path (for now)
if platform.system() == 'Windows':
    arnoldPath = 'd:\\Autodesk\\Arnold-7.2.4.1\\'
    arnoldPythonPath = '%spython' % arnoldPath
    arnoldBinPath = '%sbin' % arnoldPath
else:
    # assume Linux
    arnoldPath = '/usr/local/Arnold'
    arnoldPythonPath = '%s/python' % arnoldPath
    arnoldBinPath = '%s/bin' % arnoldPath
try:
    import arnold
except ModuleNotFoundError:
    if not (arnoldPythonPath in sys.path):
        sys.path = [arnoldPythonPath] + sys.path
    # try again
    try:
        import arnold
    except ModuleNotFoundError:
        # give up
        pass
    else:
        # success
        arnoldFound = True
else:
    # success
    arnoldFound = True

def name_compat(name):
    if name is None:
        return 'None'
    else:
        return name.replace(' ', '_')

def mesh_triangulate(me):
    import bmesh
    bm = bmesh.new()
    bm.from_mesh(me)
    bmesh.ops.triangulate(bm, faces=bm.faces)
    bm.to_mesh(me)
    bm.free()

class AssExporter:
    def __init__(self):
        self.filename = None

    def export(self, context, filename, use_shader_file, use_shader_file_only):
        def exportMaterials():
            for mat in bpy.data.materials:
                mat_wrap = node_shader_utils.PrincipledBSDFWrapper(mat) if mat else None
                if mat_wrap:
                    base_color = mat_wrap.base_color
                    utility = arnold.AiNode(universe, "utility", mat.name)
                    arnold.AiNodeSetRGB(utility,
                                        "color",
                                        base_color[0],
                                        base_color[1],
                                        base_color[2])
                else:
                    print('WARNING: ignore "%s" ...' % mat.name)
        def exportCamera():
            percent = scene.render.resolution_percentage / 100.0
            xresolution = int(scene.render.resolution_x * percent)
            yresolution = int(scene.render.resolution_y * percent)
            # xres
            arnold.AiNodeSetInt(options, "xres", xresolution)
            # yres
            arnold.AiNodeSetInt(options, "yres", yresolution)
            if ob.data.type == 'ORTHO':
                # ortho_camera
                camera = arnold.AiNode(universe,
                                       "ortho_camera",
                                       camera_name)
                # screen_window
                ortho_scale = ob.data.ortho_scale / 2.0
                arnold.AiNodeSetVec2(camera,
                                     "screen_window_min",
                                     -ortho_scale, -ortho_scale)
                arnold.AiNodeSetVec2(camera,
                                     "screen_window_max",
                                     ortho_scale, ortho_scale)
            else:
                # persp_camera
                camera = arnold.AiNode(universe,
                                       "persp_camera",
                                       camera_name)
                # fov
                aspect = xresolution / float(yresolution)
                if aspect >= 1.0:
                    fov = math.degrees(ob.data.angle)
                else:
                    fov = 2 * math.degrees(math.atan((aspect * 16.0) / lens))
                arnold.AiNodeSetFlt(camera, "fov", fov)
            # options needs a link to camera
            arnold.AiNodeSetPtr(options, "camera", camera)
            # matrix
            am = arnold.AtMatrix()
            arnold.AiM4Identity(am)
            am[0][0] = ob_mat[0][0]
            am[0][1] = ob_mat[1][0]
            am[0][2] = ob_mat[2][0]
            am[0][3] = ob_mat[3][0]
            am[1][0] = ob_mat[0][1]
            am[1][1] = ob_mat[1][1]
            am[1][2] = ob_mat[2][1]
            am[1][3] = ob_mat[3][1]
            am[2][0] = ob_mat[0][2]
            am[2][1] = ob_mat[1][2]
            am[2][2] = ob_mat[2][2]
            am[2][3] = ob_mat[3][2]
            am[3][0] = ob_mat[0][3]
            am[3][1] = ob_mat[1][3]
            am[3][2] = ob_mat[2][3]
            am[3][3] = ob_mat[3][3]
            arnold.AiNodeSetMatrix(camera, "matrix", am)
        # print('DEBUG: use_shader_file = %s' % use_shader_file)
        # print('DEBUG: use_shader_file_only = %s' % use_shader_file_only)
        self.filename = filename
        # AiBegin
        arnold.AiBegin()
        # we need an arnold universe below
        universe = arnold.AiUniverse()
        # options
        options = arnold.AiUniverseGetOptions(universe)
        # get camera (used for rendering) from scene
        scene = bpy.context.scene
        camera_name = scene.camera.name
        camera_exported = False
        # Blender objects
        EXPORT_GLOBAL_MATRIX = Matrix()
        depsgraph = context.evaluated_depsgraph_get()
        scene = context.scene
        ##objects = scene.objects
        if use_shader_file_only:
            exportMaterials()
        else:
            exportMaterials()
            print('DEBUG: export collections ...')
            for collection in bpy.data.collections:
                if collection.hide_render or collection.use_fake_user:
                    continue
                else:
                    objects = collection.objects
                    for i, ob_main in enumerate(objects):
                        # ignore dupli children
                        if ob_main.parent and ob_main.parent.instance_type in {'VERTS', 'FACES'}:
                            continue
                        obs = [(ob_main, ob_main.matrix_world)]
                        if ob_main.is_instancer:
                            obs = []
                            obs += [(dup.instance_object.original, dup.matrix_world.copy())
                                    for dup in depsgraph.object_instances
                                    if dup.parent and dup.parent.original == ob_main]
                        for ob, ob_mat in obs:
                            ob_for_convert = ob.evaluated_get(depsgraph)
                            try:
                                me = ob_for_convert.to_mesh()
                            except RuntimeError:
                                me = None
                            if me is None:
                                if ob.type == 'CAMERA':
                                    if ob.name == camera_name:
                                        exportCamera()
                                        camera_exported = True
                                    else:
                                        print('WARNING: ignore "%s" ...' % ob.name)
                                else:
                                    print('WARNING: ignore "%s" ...' % ob.name)
                                continue
                            # _must_ do this first since it re-allocs arrays
                            mesh_triangulate(me)
                            ##me.transform(EXPORT_GLOBAL_MATRIX @ ob_mat)
                            # if negative scaling, we have to invert the normals...
                            if ob_mat.determinant() < 0.0:
                                me.flip_normals()
                            me_verts = me.vertices[:]
                            # make our own list so it can be sorted to reduce context switching
                            face_index_pairs = [(face, index) for index, face in enumerate(me.polygons)]
                            # print('DEBUG: face_index_pairs = %s' % face_index_pairs)
                            # make sure there is something to write
                            if not (len(face_index_pairs) + len(me.vertices)):
                                ob_for_convert.to_mesh_clear()
                                print('WARNING: ignore "%s" ...' % ob.name)
                                continue
                            me.calc_normals_split()
                            loops = me.loops
                            materials = me.materials[:]
                            material_names = [m.name if m else None for m in materials]
                            if not materials:
                                materials = [None]
                                material_names = [name_compat(None)]
                            materials_len = len(materials)
                            # print('DEBUG: material_names = %s' % material_names)
                            name1 = ob.name
                            name2 = ob.data.name
                            if name1 == name2:
                                obnamestring = name_compat(name1)
                            else:
                                obnamestring = '%s_%s' % (name_compat(name1), name_compat(name2))
                            ##print('DEBUG: obnamestring = %s' % obnamestring)
                            # arnold
                            polymesh = arnold.AiNode(universe, "polymesh", obnamestring)
                            arnold.AiNodeSetBool(polymesh, "smoothing", True)
                            vlist = arnold.AiArrayAllocate(len(me_verts), 1,
                                                           arnold.AI_TYPE_VECTOR)
                            nlist = arnold.AiArrayAllocate(len(me_verts), 1,
                                                           arnold.AI_TYPE_VECTOR)
                            numVertexIndices = 0
                            for v in me_verts:
                                arnold.AiArraySetVec(vlist, numVertexIndices,
                                                     arnold.AtVector(v.co[0],
                                                                     v.co[1],
                                                                     v.co[2]))
                                arnold.AiArraySetVec(nlist, numVertexIndices,
                                                     arnold.AtVector(v.normal[0],
                                                                     v.normal[1],
                                                                     v.normal[2]))
                                # print('DEBUG: arnold.AiArraySetVec(vlist, %s, (%s, %s, %s))' %
                                #       (numVertexIndices, v.co[0], v.co[1], v.co[2]))
                                numVertexIndices += 1
                            arnold.AiNodeSetArray(polymesh, "vlist", vlist)
                            arnold.AiNodeSetArray(polymesh, "nlist", nlist)
                            nsides = arnold.AiArrayAllocate(len(face_index_pairs), 1,
                                                            arnold.AI_TYPE_UINT)
                            vidxs = arnold.AiArrayAllocate(len(face_index_pairs) * 3, 1, # triangles
                                                           arnold.AI_TYPE_UINT)
                            nidxs = arnold.AiArrayAllocate(len(face_index_pairs) * 3, 1, # triangles
                                                           arnold.AI_TYPE_UINT)
                            if materials_len > 1:
                                shidxs = arnold.AiArrayAllocate(len(face_index_pairs), 1,
                                                                arnold.AI_TYPE_BYTE)
                            numVertexIndices = 0 # reset
                            for f, f_index in face_index_pairs:
                                f_smooth = f.use_smooth
                                if not f_smooth:
                                    arnold.AiNodeSetBool(polymesh, "smoothing", False)
                                f_mat = min(f.material_index, materials_len - 1)
                                if materials_len > 1:
                                    if f_mat >= materials_len:
                                        print('ERROR: fmat(%s) >= materials_len(%s)' %
                                              (f_mat, materials_len))
                                        arnold.AiArraySetByte(shidxs, f_index, 0)
                                    else:
                                        arnold.AiArraySetByte(shidxs, f_index, f_mat)
                                f_v = [(vi, me_verts[v_idx], l_idx)
                                       for vi, (v_idx, l_idx) in enumerate(zip(f.vertices, f.loop_indices))]
                                # print('DEBUG: f_v = %s' % f_v)
                                for vi, v, li in f_v:
                                    # vidxs
                                    arnold.AiArraySetUInt(vidxs, numVertexIndices, v.index)
                                    # nidxs
                                    arnold.AiArraySetUInt(nidxs, numVertexIndices, v.index)
                                    # print('DEBUG: arnold.AiArraySetUInt(vidxs, %s, %s)' %
                                    #       (numVertexIndices, v.index))
                                    numVertexIndices += 1
                                arnold.AiArraySetUInt(nsides, f_index, 3) # triangle
                                # print('DEBUG: arnold.AiArraySetUInt(nsides, %s, 3)' % f_index)
                            arnold.AiNodeSetArray(polymesh, "nsides", nsides)
                            arnold.AiNodeSetArray(polymesh, "vidxs", vidxs)
                            arnold.AiNodeSetArray(polymesh, "nidxs", nidxs)
                            am = arnold.AtMatrix()
                            arnold.AiM4Identity(am)
                            am[0][0] = ob_mat[0][0]
                            am[0][1] = ob_mat[1][0]
                            am[0][2] = ob_mat[2][0]
                            am[0][3] = ob_mat[3][0]
                            am[1][0] = ob_mat[0][1]
                            am[1][1] = ob_mat[1][1]
                            am[1][2] = ob_mat[2][1]
                            am[1][3] = ob_mat[3][1]
                            am[2][0] = ob_mat[0][2]
                            am[2][1] = ob_mat[1][2]
                            am[2][2] = ob_mat[2][2]
                            am[2][3] = ob_mat[3][2]
                            am[3][0] = ob_mat[0][3]
                            am[3][1] = ob_mat[1][3]
                            am[3][2] = ob_mat[2][3]
                            am[3][3] = ob_mat[3][3]
                            arnold.AiNodeSetMatrix(polymesh, "matrix", am)
                            # shader
                            if materials_len == 1:
                                material = materials[0]
                                if material != None:
                                    material_name = material.name
                                    shader = arnold.AiNodeLookUpByName(universe, material_name)
                                    if shader != None:
                                        arnold.AiNodeSetPtr(polymesh, "shader", shader)
                                    else:
                                        print('WARNING: ignore "%s" material ...' % mat.name)
                                else:
                                    print('WARNING: ignore material for "%s" ...' % obnamestring)
                            else:
                                counter = 0
                                shaders = arnold.AiArray(materials_len,
                                                         1,
                                                         arnold.AI_TYPE_NODE)
                                for mat_idx, material in enumerate(materials):
                                    if material != None:
                                        material_name = material.name
                                        shader = arnold.AiNodeLookUpByName(universe, material_name)
                                        if shader != None:
                                            arnold.AiArraySetPtr(shaders, mat_idx, shader)
                                            counter += 1
                                        else:
                                            print('WARNING: ignore "%s" material[%s] ...' %
                                                  (mat.name, mat_idx))
                                    else:
                                        print('WARNING: ignore material[%s] for "%s" ...' %
                                              (mat_idx, obnamestring))
                                if counter == materials_len:
                                    arnold.AiNodeSetArray(polymesh, "shidxs", shidxs)
                                    arnold.AiNodeSetArray(polymesh,
                                                          "shader",
                                                          shaders)
                        #     print(polymesh)
                        # print(ob_main)
        if not camera_exported and len(bpy.data.cameras) >= 1:
            # camera was not part of a collection
            objects = bpy.data.objects
            for i, ob_main in enumerate(objects):
                obs = [(ob_main, ob_main.matrix_world)]
                for ob, ob_mat in obs:
                    if ob.name == camera_name:
                        print('DEBUG: export camera ...')
                        exportCamera()
        if not use_shader_file_only:
            # AiSceneWrite
            params = arnold.AiParamValueMap()
            if use_shader_file:
                mask = arnold.AI_NODE_ALL & ~arnold.AI_NODE_SHADER
            else:
                mask = arnold.AI_NODE_ALL
            arnold.AiParamValueMapSetInt(params, "mask", mask)
            arnold.AiParamValueMapSetBool(params, "binary", False)
            arnold.AiSceneWrite(universe, filename, params)
            params = arnold.AiParamValueMap()
        if use_shader_file:
            # shader_filename
            shader_filename, ext = os.path.splitext(filename)
            shader_filename = shader_filename + '_shaders' + ext
            # AiSceneWrite
            params = arnold.AiParamValueMap()
            mask = arnold.AI_NODE_SHADER # shader nodes
            arnold.AiParamValueMapSetInt(params, "mask", mask)
            arnold.AiParamValueMapSetBool(params, "binary", False)
            arnold.AiSceneWrite(universe, shader_filename, params)
        # AiEnd
        arnold.AiEnd()

def save(context,
         filepath,
         *,
         use_shader_file=False,
         use_shader_file_only=False,
         use_triangles=False
         ):
    if arnoldFound:
        if platform.system() == 'Windows':
            ldLibraryPathOk = True
        else:
            # LD_LIBRARY_PATH
            ldLibraryPathOk = False
            try:
                ldLibraryPath = os.environ['LD_LIBRARY_PATH']
            except KeyError:
                print("ERROR: LD_LIBRARY_PATH not set!")
            else:
                words = ldLibraryPath.split(':')
                if arnoldBinPath in words:
                    ldLibraryPathOk = True
        if ldLibraryPathOk:
            # export
            assExporter = AssExporter()
            assExporter.export(context, filepath, use_shader_file, use_shader_file_only)
        else:
            print("ERROR: $ARNOLD/bin must be in LD_LIBRARY_PATH!")
    else:
        print("ERROR: Can't import arnold module!")
    return {'FINISHED'}
