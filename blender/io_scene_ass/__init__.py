# SPDX-License-Identifier: GPL-2.0-or-later

bl_info = {
    "name": "Export Arnold ASS Format (.ass)",
    "author": "Jan Walter",
    "version": (0, 0, 1),
    "blender": (3, 5, 0),
    "location": "File > Export > Arnold ASS",
    "description": "The script exports Blender geometry to Arnold's ASS format.",
    "warning": "Under construction!",
    "doc_url": "https://codeberg.org/wahn/Arnold",
    "category": "Import-Export",
}

if "bpy" in locals():
    import importlib
    # if "import_ass" in locals():
    #     importlib.reload(import_ass)
    if "export_ass" in locals():
        importlib.reload(export_ass)

import bpy
from bpy.props import (
    BoolProperty,
    StringProperty,
)
from bpy_extras.io_utils import (
    # ImportHelper,
    ExportHelper,
)

# class ImportASS(bpy.types.Operator, ImportHelper):
#     """Load a Arnold ASS File"""

#     bl_idname = "import_scene.ass"
#     bl_label = "Import ASS"
#     bl_options = {'PRESET', 'UNDO'}

#     filename_ext = ".ass"
#     pass

#     def execute(self, context):
#         pass

#     def draw(self, context):
#         pass

class ExportASS(bpy.types.Operator, ExportHelper):
    """Save a Arnold ASS File"""

    bl_idname = "export_scene.ass"
    bl_label = 'Export ASS'
    bl_options = {'PRESET'}

    filename_ext = ".ass"
    filter_glob: StringProperty(
        default="*.ass",
        options={'HIDDEN'},
    )
    # context group
    use_shader_file: BoolProperty(
        name="Shader File",
        description="Export additional shader .ass file (named *_shaders.ass)",
        default=False,
    )
    use_shader_file_only: BoolProperty(
        name="Use Shader File Only",
        description="Omit writing the main .ass file",
        default=False,
    )
    # extra data group
    use_triangles: BoolProperty(
        name="Triangulate Faces",
        description="Convert all faces to triangles",
        default=False,
    )
    check_extension = True

    def execute(self, context):
        from . import export_ass
        keywords = self.as_keywords(
            ignore=(
                "check_existing",
                "filter_glob",
            ),
        )
        return export_ass.save(context, **keywords)

    def draw(self, context):
        pass

class ASS_PT_export_include(bpy.types.Panel):
    bl_space_type = 'FILE_BROWSER'
    bl_region_type = 'TOOL_PROPS'
    bl_label = "Include"
    bl_parent_id = "FILE_PT_operator"

    @classmethod
    def poll(cls, context):
        sfile = context.space_data
        operator = sfile.active_operator

        return operator.bl_idname == "EXPORT_SCENE_OT_ass"

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False  # No animation.

        sfile = context.space_data
        operator = sfile.active_operator

        col = layout.column(heading="Shader")
        col.prop(operator, 'use_shader_file')
        col.prop(operator, 'use_shader_file_only')

# def menu_func_import(self, context):
#     self.layout.operator(ImportASS.bl_idname, text="Arnold (.ass)")

def menu_func_export(self, context):
    self.layout.operator(ExportASS.bl_idname, text="Arnold (.ass)")


classes = (
    # ImportASS,
    ExportASS,
    ASS_PT_export_include,
)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    # bpy.types.TOPBAR_MT_file_import.append(menu_func_import)
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)

def unregister():
    # bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)

    for cls in classes:
        bpy.utils.unregister_class(cls)

if __name__ == "__main__":
    register()
