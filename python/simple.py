#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import arnold

if __name__ == "__main__":
    arnold.AiSetAppString("simple.py 1.0.0")
    arnold.AiBegin()
    arnold.AiASSWrite("simple.ass", arnold.AI_NODE_ALL, False, False)
    arnold.AiEnd()
