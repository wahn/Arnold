ass_file = 'cornell_box.ass'
# render (or not)?
render = False
render = True
# where is Arnold?
arnold_path = '/usr/local/Arnold'
import os
arnold_python_path = os.path.join(arnold_path, 'python')
arnold_bin_path = os.path.join(arnold_path, 'bin')
# on Linux the LD_LIBRARY_PATH must be set to find libai.so
import sys
try:
    print('DEBUG: LD_LIBRARY_PATH=%s' % os.environ['LD_LIBRARY_PATH'])
except KeyError:
    print('TODO: export LD_LIBRARY_PATH=%s' % arnold_bin_path)
    sys.exit()
# make sure arnold Python module can be found
if not (arnold_python_path in sys.path):
    print('DONE: add "%s" to sys.path ...' % arnold_python_path)
    sys.path = [arnold_python_path] + sys.path
# load arnold Python module
import arnold
# AiBegin
arnold.AiBegin()
# load .ass file
universe = arnold.AiUniverse()
params = arnold.AiParamValueMap()
arnold.AiParamValueMapSetInt(params, 'mask', arnold.AI_NODE_ALL)
arnold.AiSceneLoad(universe, ass_file, params)
# new driver
print('=' * 79)
new_driver = "driver_png"
print('DEBUG: create new driver: "%s"' % new_driver)
driver = arnold.AiNode(universe, new_driver, new_driver)
arnold.AiNodeSetStr(driver, "filename", "arnold.png")
numOutputs = 1 # beauty
new_outputs = arnold.AiArrayAllocate(numOutputs, 1, arnold.AI_TYPE_STRING)
arnold.AiArraySetStr(new_outputs, 0, "RGBA RGBA filter driver_png")
# options node
node = arnold.AiNodeLookUpByName(universe, 'options')
node_entry = arnold.AiNodeGetNodeEntry(node)
param_entry = arnold.AiNodeEntryLookUpParameter(node_entry, 'outputs')
if arnold.AiParamGetType(param_entry) == arnold.AI_TYPE_ARRAY:
    outputs = arnold.AiNodeGetArray(node, 'outputs')
    num_elements = arnold.AiArrayGetNumElements(outputs)
    print('DEBUG: %s %s %s %s' %
          (arnold.AiParamGetName(param_entry),
           num_elements,
           arnold.AiArrayGetNumKeys(outputs),
           arnold.AiParamGetTypeName(arnold.AiParamGetSubType(param_entry))))
    for idx in range(num_elements):
        print('DEBUG: outputs[%s] = "%s"' % (idx, arnold.AiArrayGetStr(outputs, idx)))
    # replace outputs by new_outputs (using new driver)
    arnold.AiNodeSetArray(node, "outputs", new_outputs)
print('=' * 79)
if render:
    # AiRender
    render_session = arnold.AiRenderSession(universe)
    arnold.AiRender(render_session)
# AiEnd
arnold.AiEnd()
