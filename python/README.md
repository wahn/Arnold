<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#org604ed1f">1. Arnold and Python</a>
<ul>
<li><a href="#orgdf36012">1.1. Environment</a></li>
<li><a href="#orgaa1f662">1.2. Within the Python interpreter</a></li>
<li><a href="#orga4d67ec">1.3. Simple Python script</a></li>
</ul>
</li>
</ul>
</div>
</div>


<a id="org604ed1f"></a>

# Arnold and Python


<a id="orgdf36012"></a>

## Environment

First set `ARNOLD_ROOT`:

    setenv ARNOLD_ROOT /mill3d/server/apps/ARNOLD/linux-x86-64/Arnold-6.0.1.0

Then set the `PYTHONPATH`:

    setenv PYTHONPATH ${ARNOLD_ROOT}/python

Finally there might be problems to find e.g. the `libai.so` by loaded
plugins or shaders, so we should extend the `LD_LIBRARY_PATH`:

    setenv LD_LIBRARY_PATH $ARNOLD_ROOT/bin:$LD_LIBRARY_PATH
    ldd /mill3d/server/apps/ARNOLD/linux-x86-64/Arnold-6.0.1.0/bin/../plugins/alembic_proc.so
      linux-vdso.so.1 =>  (0x00007fff11bc4000)
      libai.so => /mill3d/server/apps/ARNOLD/linux-x86-64/Arnold-6.0.1.0/bin/libai.so (0x00007f9011dd7000)
      libstdc++.so.6 => /lib64/libstdc++.so.6 (0x00007f9011acf000)
      libm.so.6 => /lib64/libm.so.6 (0x00007f90117cd000)
      libgcc_s.so.1 => /lib64/libgcc_s.so.1 (0x00007f90115b7000)
      libc.so.6 => /lib64/libc.so.6 (0x00007f90111ea000)
      libAdClmHub.so.1 => /mill3d/server/apps/ARNOLD/linux-x86-64/Arnold-6.0.1.0/bin/libAdClmHub.so.1 (0x00007f9010f42000)
      libAdskLicensingSDK.so.2 => /mill3d/server/apps/ARNOLD/linux-x86-64/Arnold-6.0.1.0/bin/libAdskLicensingSDK.so.2 (0x00007f90106fd000)
      librt.so.1 => /lib64/librt.so.1 (0x00007f90104f5000)
      libdl.so.2 => /lib64/libdl.so.2 (0x00007f90102f1000)
      libpthread.so.0 => /lib64/libpthread.so.0 (0x00007f90100d5000)
      libAdpSDKWrapper-arnold.so => /mill3d/server/apps/ARNOLD/linux-x86-64/Arnold-6.0.1.0/bin/libAdpSDKWrapper-arnold.so (0x00007f900fe53000)
      liboptix.so.6.5.0 => /mill3d/server/apps/ARNOLD/linux-x86-64/Arnold-6.0.1.0/bin/liboptix.so.6.5.0 (0x00007f900fb62000)
      /lib64/ld-linux-x86-64.so.2 (0x00007f90174d9000)
      libuuid.so.1 => /lib64/libuuid.so.1 (0x00007f900f95d000)
      libattr.so.1 => /lib64/libattr.so.1 (0x00007f900f758000)


<a id="orgaa1f662"></a>

## Within the Python interpreter

Just to check that we are ready to use the Arnold API from within [Python](https://www.python.org):

    python
    Python 2.7.5 (default, Apr 11 2018, 07:36:10) 
    [GCC 4.8.5 20150623 (Red Hat 4.8.5-28)] on linux2
    Type "help", "copyright", "credits" or "license" for more information.
    >>> import arnold


<a id="orga4d67ec"></a>

## Simple Python script

    #!/usr/bin/env python3
    # -*- coding: utf-8 -*-
    
    import arnold
    
    if __name__ == "__main__":
        arnold.AiSetAppString("simple.py 1.0.0")
        arnold.AiBegin()
        arnold.AiASSWrite("simple.ass", arnold.AI_NODE_ALL, False, False)
        arnold.AiEnd()

