import bpy
import sys

class ReadAssGeometry(bpy.types.Operator):
    bl_idname = "object.read_ass_geometry_operator"
    bl_label = "Read geometry from .ass file"

    def execute(self, context):
        # import arnold
        try:
            import arnold
        except ImportError:
            arnold_python = 'D:\\Autodesk\\Arnold-6.2.1.1\\python'
            if not (arnold_python in sys.path):
                print('adding "%s" to sys.path ...' % arnold_python)
                sys.path.append(arnold_python)
            # try again
            import arnold
        # AiBegin
        arnold.AiBegin()
        # load .ass file
        ass_file = 'e:\\blender\\ass\\BFr_Bonnet.ass'
        print('loading "%s" ...' % ass_file)
        arnold.AiASSLoad(ass_file)
        # iterate over all shape nodes
        iter = arnold.AiUniverseGetNodeIterator(arnold.AI_NODE_SHAPE)
        counter = 0
        while not arnold.AiNodeIteratorFinished(iter):
            node = arnold.AiNodeIteratorGetNext(iter)
            # name
            name = arnold.AiNodeGetStr(node, 'name')
            print('%s: %s' % (counter, name))
            if name != 'root':
                # nsides
                nsides = arnold.AiNodeGetArray(node, 'nsides')
                if nsides != None:
                    num_elements = arnold.AiArrayGetNumElements(nsides)
                    num_keys = arnold.AiArrayGetNumKeys(nsides)
                    print(' nsides %s %s UINT' % (num_elements, num_keys))
                    if num_keys != 1:
                        print('WARNING: "%s" has %s keys' % ('nsides', num_keys))
                    nsides_values = []
                    for index in range(num_elements):
                        nsides_values.append(arnold.AiArrayGetUInt(nsides, index))
                    print('  [%s ... %s]' % (nsides_values[0], nsides_values[-1]))
                # vidxs
                vidxs = arnold.AiNodeGetArray(node, 'vidxs')
                if vidxs != None:
                    num_elements = arnold.AiArrayGetNumElements(vidxs)
                    num_keys = arnold.AiArrayGetNumKeys(vidxs)
                    print(' vidxs %s %s UINT' % (num_elements, num_keys))
                    if num_keys != 1:
                        print('WARNING: "%s" has %s keys' % ('vidxs', num_keys))
                    vidxs_values = []
                    for index in range(num_elements):
                        vidxs_values.append(arnold.AiArrayGetUInt(vidxs, index))
                    print('  [%s ... %s]' % (vidxs_values[0], vidxs_values[-1]))
                # nidxs
                nidxs = arnold.AiNodeGetArray(node, 'nidxs')
                if nidxs != None:
                    num_elements = arnold.AiArrayGetNumElements(nidxs)
                    num_keys = arnold.AiArrayGetNumKeys(nidxs)
                    print(' nidxs %s %s UINT' % (num_elements, num_keys))
                    if num_keys != 1:
                        print('WARNING: "%s" has %s keys' % ('nidxs', num_keys))
                    nidxs_values = []
                    for index in range(num_elements):
                        nidxs_values.append(arnold.AiArrayGetUInt(nidxs, index))
                    print('  [%s ... %s]' % (nidxs_values[0], nidxs_values[-1]))
                # uvidxs
                uvidxs = arnold.AiNodeGetArray(node, 'uvidxs')
                if uvidxs != None:
                    num_elements = arnold.AiArrayGetNumElements(uvidxs)
                    num_keys = arnold.AiArrayGetNumKeys(uvidxs)
                    print(' uvidxs %s %s UINT' % (num_elements, num_keys))
                    if num_keys != 1:
                        print('WARNING: "%s" has %s keys' % ('uvidxs', num_keys))
                    uvidxs_values = []
                    for index in range(num_elements):
                        uvidxs_values.append(arnold.AiArrayGetUInt(uvidxs, index))
                    print('  [%s ... %s]' % (uvidxs_values[0], uvidxs_values[-1]))
                # vlist
                vlist = arnold.AiNodeGetArray(node, 'vlist')
                if vlist != None:
                    num_elements = arnold.AiArrayGetNumElements(vlist)
                    num_keys = arnold.AiArrayGetNumKeys(vlist)
                    print(' vlist %s %s VECTOR' % (num_elements, num_keys))
                    if num_keys != 1:
                        print('WARNING: "%s" has %s keys' % ('vlist', num_keys))
                    vlist_values = []
                    for index in range(num_elements):
                        vlist_values.append(arnold.AiArrayGetVec(vlist, index))
                    print('  [%10.7f %10.7f %10.7f ... %10.7f %10.7f %10.7f]' % (vlist_values[0].x,
                                                                                 vlist_values[0].y,
                                                                                 vlist_values[0].z,
                                                                                 vlist_values[-1].x,
                                                                                 vlist_values[-1].y,
                                                                                 vlist_values[-1].z))
                # nlist
                nlist = arnold.AiNodeGetArray(node, 'nlist')
                if nlist != None:
                    num_elements = arnold.AiArrayGetNumElements(nlist)
                    num_keys = arnold.AiArrayGetNumKeys(nlist)
                    print(' nlist %s %s VECTOR' % (num_elements, num_keys))
                    if num_keys != 1:
                        print('WARNING: "%s" has %s keys' % ('nlist', num_keys))
                    nlist_values = []
                    for index in range(num_elements):
                        nlist_values.append(arnold.AiArrayGetVec(nlist, index))
                    print('  [%10.7f %10.7f %10.7f ... %10.7f %10.7f %10.7f]' % (nlist_values[0].x,
                                                                                 nlist_values[0].y,
                                                                                 nlist_values[0].z,
                                                                                 nlist_values[-1].x,
                                                                                 nlist_values[-1].y,
                                                                                 nlist_values[-1].z))
                # uvlist
                uvlist = arnold.AiNodeGetArray(node, 'uvlist')
                if uvlist != None:
                    num_elements = arnold.AiArrayGetNumElements(uvlist)
                    num_keys = arnold.AiArrayGetNumKeys(uvlist)
                    print(' uvlist %s %s VECTOR2' % (num_elements, num_keys))
                    if num_keys != 1:
                        print('WARNING: "%s" has %s keys' % ('uvlist', num_keys))
                    uvlist_values = []
                    for index in range(num_elements):
                        uvlist_values.append(arnold.AiArrayGetVec2(uvlist, index))
                    print('  [%10.7f %10.7f ... %10.7f %10.7f]' % (uvlist_values[0].x,
                                                                   uvlist_values[0].y,
                                                                   uvlist_values[-1].x,
                                                                   uvlist_values[-1].y))
                # polymesh
                # polymesh = Polymesh(ass_name,
                #                     nsides_values,
                #                     vidxs_values,
                #                     nidxs_values,
                #                     uvidxs_values,
                #                     vlist_values,
                #                     nlist_values,
                #                     uvlist_values)
                # print('-' * 79)
                # print(polymesh)
                # print('-' * 79)
            counter += 1
        # AiEnd
        arnold.AiEnd()
        return {'FINISHED'}

bpy.utils.register_class(ReadAssGeometry)
