<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#org5824bac">1. Environment</a></li>
<li><a href="#org28a62ad">2. Useful command line options</a></li>
<li><a href="#org723641e">3. Arnold and Python</a></li>
<li><a href="#org0b2cb5e">4. Arnold and C/C++</a>
<ul>
<li><a href="#org5de5ea3">4.1. Compile the example C code:</a></li>
<li><a href="#org891db6f">4.2. Run the resulting executable</a></li>
</ul>
</li>
</ul>
</div>
</div>


<a id="org5824bac"></a>

# Environment

    setenv ARNOLD_ROOT /mill3d/server/apps/ARNOLD/linux-x86-64/Arnold-6.0.1.0
    setenv LD_LIBRARY_PATH $ARNOLD_ROOT/bin:$LD_LIBRARY_PATH


<a id="org28a62ad"></a>

# Useful command line options

    kick --help
    ...
    Usage:  kick [options] ...
      -i %s               Input .ass file
      -o %s               Output filename
      -of %s              Output format: exr jpg png tif 
    ...
      -as %d              Anti-aliasing samples
    ...
      -bs %d              Bucket size
      -bc %s              Bucket scanning (top left random spiral hilbert)
      -td %d              Total ray depth
      -dif %d             Diffuse depth
      -spc %d             Specular depth
      -trm %d             Transmission depth
      -ds %d              Diffuse samples
      -ss %d              Specular samples
      -ts %d              Transmission samples
    ...
      -dp                 Disable progressive rendering (recommended for batch rendering)
    ...
      -v %d               Verbose level (0..6)
    ...
      -nodes [n|t]        List all installed nodes, sorted by Name (default) or Type
      -info [n|u] %s      Print detailed information for a given node, sorted by Name or Unsorted (default)
    ...
      -resave %s          Re-save .ass scene to filename
      -db                 Disable binary encoding when re-saving .ass files (useful for debugging)
    ...
      -licensecheck       Check the connection with the license servers and list installed licenses
    ...


<a id="org723641e"></a>

# Arnold and Python

Running the simple example script:

    cd python/
    python3.8 simple.py
    00:00:00    75MB WARNING | detected unknown memory allocator loaded from "python3.8".  This might not be compatible with arnold and might result in performance regressions, increased memory usage, and crashes! If it must be used, please contact support@solidangle.com so we can whitelist this allocator
    00:00:00    75MB         | log started Wed Dec 18 12:47:52 2019
    00:00:00    75MB         | Arnold 6.0.1.0 [25372a4c] linux clang-5.0.0 oiio-2.1.4 osl-1.11.0 vdb-4.0.0 clm-1.1.1.118 rlm-12.4.2 optix-6.5.0 2019/12/04 07:45:07
    00:00:00    75MB         | host application: simple.py 1.0.0
    00:00:00    75MB         | running on rnd12.mill.co.uk, pid=14908
    00:00:00    75MB         |  2 x Intel(R) Xeon(R) CPU E5620 @ 2.40GHz (8 cores, 8 logical) with 48119MB
    00:00:00    75MB         |  NVIDIA driver version 418.43 (Optix 0)
    00:00:00    75MB         |  CentOS Linux 7 (Core), Linux kernel 3.10.0-862.2.3.el7.x86_64
    00:00:00    75MB         |  soft limit for open files raised from 8192 to 8190
    00:00:00    75MB         |  
    00:00:00    75MB         | loading plugins from /mill3d/server/apps/ARNOLD/linux-x86-64/Arnold-6.0.1.0/bin/../plugins ...
    00:00:00    75MB         | loaded 1 plugins from 1 lib(s) in 0:00.00
    00:00:00    75MB         | [ass] writing scene to simple.ass (mask=0xFFFF) ...
    00:00:00    76MB         | [ass] wrote 232 bytes, 1 nodes in 0:00.00
    00:00:00    76MB         |  
    00:00:00    76MB         | releasing resources
    00:00:00    76MB         | Arnold shutdown

This will create a `simple.ass` file:

    cat simple.ass 
    ### exported: Wed Dec 18 12:47:52 2019
    ### from:     Arnold 6.0.1.0 [25372a4c] linux clang-5.0.0 oiio-2.1.4 osl-1.11.0 vdb-4.0.0 clm-1.1.1.118 rlm-12.4.2 optix-6.5.0 2019/12/04 07:45:07
    ### host app: simple.py 1.0.0
    
    
    
    options
    {
    }


<a id="org0b2cb5e"></a>

# Arnold and C/C++


<a id="org5de5ea3"></a>

## Compile the example C code:

    cd cpp/
    make
    g++ -std=c++11 -Wall -O2 -o simple -I/mill3d/server/apps/ARNOLD/linux-x86-64/Arnold-6.0.1.0/include -L/mill3d/server/apps/ARNOLD/linux-x86-64/Arnold-6.0.1.0/bin -lai simple.c


<a id="org891db6f"></a>

## Run the resulting executable

    ./simple 
    00:00:00    64MB         | log started Wed Dec 18 13:35:49 2019
    00:00:00    64MB         | Arnold 6.0.1.0 [25372a4c] linux clang-5.0.0 oiio-2.1.4 osl-1.11.0 vdb-4.0.0 clm-1.1.1.118 rlm-12.4.2 optix-6.5.0 2019/12/04 07:45:07
    00:00:00    64MB         | host application: simple.c 1.0.0
    00:00:00    64MB         | running on rnd12.mill.co.uk, pid=3124
    00:00:00    64MB         |  2 x Intel(R) Xeon(R) CPU E5620 @ 2.40GHz (8 cores, 8 logical) with 48119MB
    00:00:00    64MB         |  NVIDIA driver version 418.43 (Optix 0)
    00:00:00    64MB         |  CentOS Linux 7 (Core), Linux kernel 3.10.0-862.2.3.el7.x86_64
    00:00:00    64MB         |  soft limit for open files raised from 8192 to 8190
    00:00:00    64MB         |  
    00:00:00    64MB         | loading plugins from /mill3d/server/apps/ARNOLD/linux-x86-64/Arnold-6.0.1.0/bin/../plugins ...
    00:00:00    64MB         | loaded 1 plugins from 1 lib(s) in 0:00.00
    00:00:00    64MB         | [ass] writing scene to simple.ass (mask=0xFFFF) ...
    00:00:00    64MB         | [ass] wrote 231 bytes, 1 nodes in 0:00.00
    00:00:00    64MB         |  
    00:00:00    64MB         | releasing resources
    00:00:00    64MB         | Arnold shutdown

This will create a `simple.ass` file:

    cat simple.ass 
    ### exported: Wed Dec 18 13:35:49 2019
    ### from:     Arnold 6.0.1.0 [25372a4c] linux clang-5.0.0 oiio-2.1.4 osl-1.11.0 vdb-4.0.0 clm-1.1.1.118 rlm-12.4.2 optix-6.5.0 2019/12/04 07:45:07
    ### host app: simple.c 1.0.0
    
    
    
    options
    {
    }

