<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#org9bc7690">1. Arnold and C/C++</a>
<ul>
<li><a href="#org1d45239">1.1. Environment</a></li>
<li><a href="#orgf5dae80">1.2. The C code</a></li>
<li><a href="#orge44c676">1.3. Compiling</a></li>
</ul>
</li>
</ul>
</div>
</div>


<a id="org9bc7690"></a>

# Arnold and C/C++


<a id="org1d45239"></a>

## Environment

First set `ARNOLD_ROOT`:

    setenv ARNOLD_ROOT /mill3d/server/apps/ARNOLD/linux-x86-64/Arnold-6.0.1.0

After creating an executable the `libai.so` has to be found, therefore
lets extend the `LD_LIBRARY_PATH`:

    setenv LD_LIBRARY_PATH $ARNOLD_ROOT/bin:$LD_LIBRARY_PATH


<a id="orgf5dae80"></a>

## The C code

The simplest C program using the [Arnold](https://www.arnoldrenderer.com/) API looks like this:

    #include <ai.h>
    #include <ai_dotass.h>
    #include <ai_node_entry.h>
    #include <ai_render.h>
    #include <ai_version.h>
    
    int main() {
      AiSetAppString("simple.c 1.0.0");
      AiBegin();
      AiASSWrite("simple.ass", AI_NODE_ALL, false, false);
      AiEnd();
      return 0;
    }


<a id="orge44c676"></a>

## Compiling

To compile the C file into an executable called `simple` lets use this
`Makefile`:

    all: simple
    
    CC = g++ -std=c++11 -Wall -O2
    ARNOLD_ROOT = /mill3d/server/apps/ARNOLD/linux-x86-64/Arnold-6.0.1.0
    
    simple: simple.c
      $(CC) -o simple -I$(ARNOLD_ROOT)/include -L$(ARNOLD_ROOT)/bin -lai simple.c
    
    clean:
      -rm -f *~ simple simple.ass

