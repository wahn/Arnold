#include <ai.h>
#include <ai_dotass.h>
#include <ai_node_entry.h>
#include <ai_render.h>
#include <ai_version.h>

int main() {
  AiSetAppString("simple.cpp 1.0.0");
  AiBegin();
  AtUniverse* universe = AiUniverse();
  AtParamValueMap* params = AiParamValueMap();
  AiParamValueMapSetInt(params, AtString("mask"), AI_NODE_ALL);
  AiSceneWrite(universe, "simple.ass", params);
  AiEnd();
  return 0;
}
